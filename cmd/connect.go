package cmd

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/spf13/cobra"
	"golang.org/x/term"
)

type RDPCon struct {
	user       string
	password   string
	domain     string
	server     string
	port       string
	mount      string
	resolution string
	sound      string
	microphone bool
}

var rdpcon RDPCon

var connectCmd = &cobra.Command{
	Use:   "connect [ssh-like|verbose]",
	Short: "Connects to a remote Windows machine through RDP",
	Long: `To stablish a connection two syntax can be used:
ssh-like:
	user@server
verbose:
	as USER to SERVER
	to SERVER as USER`,
	RunE: func(cmd *cobra.Command, args []string) error {

		if len(args) == 1 {
			err := sshCon(args, &rdpcon)
			if err != nil {
				return err
			}

		} else if len(args) == 4 {
			err := verboseCon(args, &rdpcon)
			if err != nil {
				return err
			}

		} else {
			return errors.New("missing information")
		}

		err := winConnect()
		if err != nil {
			return err
		} else {
			return nil
		}
	},
}

/*
 * xfreerdp plugins: https://github.com/FreeRDP/FreeRDP/wiki/Plugins
 */
func init() {
	rootCmd.AddCommand(connectCmd)
	// domain
	connectCmd.Flags().StringVarP(&rdpcon.domain, "domain", "D", "", "Domain")

	// port
	connectCmd.Flags().StringVarP(&rdpcon.resolution, "resolution", "r", "", "Sets the connection resolution\nExample: --resolution 1024x768 (default resolution)\n")

	//resolution
	connectCmd.Flags().StringVarP(&rdpcon.port, "port", "p", "", "Sets the connection port\n")

	// mount
	connectCmd.Flags().StringVarP(&rdpcon.mount, "mount", "M", "", "mounts a local folder to the remote windows target\nSyntax: --mount remoteName,localPath\n")

	// sound
	connectCmd.Flags().StringVarP(&rdpcon.sound, "sound", "s", "", "Enable sound on [local|remote] host. If none is chosen then sound will not be available in the RDP session")

	// microphone
	connectCmd.Flags().BoolVarP(&rdpcon.microphone, "enable-mic", "m", false, "Enable microphone redirection to remote host. Defaults to false")

}

func sshCon(args []string, rdpCon *RDPCon) error {
	sep := "@"

	if strings.Contains(args[0], sep) {
		slices := strings.Split(args[0], sep)
		rdpCon.user = slices[0]
		rdpCon.server = slices[1]

	} else {
		return errors.New("not an SSH-like syntax")
	}

	return nil
}

func verboseCon(args []string, rdpCon *RDPCon) error {
	if args[0] == "as" && args[2] == "to" {
		rdpCon.user = args[1]
		rdpCon.server = args[3]

	} else if args[0] == "to" && args[2] == "as" {
		rdpCon.user = args[3]
		rdpCon.server = args[1]

	} else {
		return errors.New("verbose connection keywords not found")
	}

	return nil
}

func cmdCon() []string {
	baseCon := []string{
		"xfreerdp",
		"+clipboard",
		"/cert-tofu",
		"/dynamic-resolution",
		"/u:" + rdpcon.user,
		"/p:" + rdpcon.password,
		"/v:" + rdpcon.server}

	if rdpcon.domain != "" {
		baseCon = append(baseCon, "/d:"+rdpcon.domain)
	}

	if rdpcon.port != "" {
		baseCon = append(baseCon, "/port:"+rdpcon.port)
	}

	if rdpcon.mount != "" {
		baseCon = append(baseCon, "/drive:"+rdpcon.mount)
	}

	if rdpcon.resolution != "" {
		baseCon = append(baseCon, "/size:"+rdpcon.resolution)
	}

	if rdpcon.sound == "local" {
		baseCon = append(baseCon, "/audio-mode:0")
	} else if rdpcon.sound == "remote" {
		baseCon = append(baseCon, "/audio-mode:1")
	}

	if rdpcon.microphone {
		// this is "normal" behaviour
		//baseCon = append(baseCon, "/mic")

		// woraround for https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/756
		baseCon = append(baseCon, "/mic:sys:pulse,format:1")
	}

	return baseCon
}

func isXfreerdpInstalled() error {
	// verifies that xfreerdp is installed on the system
	isXfreerdpInstalled := exec.Command("which", "xfreerdp")
	err := isXfreerdpInstalled.Run()
	if err != nil {
		return errors.New("winRDP depends on xfreerdp, make sure xfreerdp is installed")
	}
	return nil

}

func winConnect() error {
	err := isXfreerdpInstalled()
	if err != nil {
		return err
	}

	//Password request
	fmt.Print("Password: ")
	pass, err := term.ReadPassword(int(os.Stdin.Fd()))
	if err != nil {
		return err
	}

	rdpcon.password = string(pass)
	fmt.Println()

	// put together xfreerdp command and arguments
	rdpCon := exec.Command(cmdCon()[0], cmdCon()[1:]...)

	/* Start() does what I need to but it doesn't provide
		monitor for an exit code in case an error happens
	   Run() will lock WinRDP until the connection is closed
		but it will provide exit code monitoring
	*/

	// https://stackoverflow.com/questions/1877045/how-do-you-get-the-output-of-a-system-command-in-go/15815730#15815730
	//err = rdpCon.Start()
	//err = rdpCon.Run()
	_, err = rdpCon.Output()
	if err != nil {
		return err
	}
	return err
}
